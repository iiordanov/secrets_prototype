FROM ubuntu:20.04

ENV SECRETS="{}"

RUN apt update && apt install -yy jq && apt autoremove && apt clean

COPY unpack_secrets.sh /
COPY command.sh /

# Entrypoint will unpack secrets from a json string set in environment variable called SECRETS
ENTRYPOINT [ "./unpack_secrets.sh" ]
# Command in this form will be passed as an argument to ENTRYPOINT and will be exec-ed by ENTRYPOINT
CMD [ "./command.sh" ]
