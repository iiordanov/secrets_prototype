#!/bin/bash

echo "All environment variables available to $0:"
echo
env
echo

echo
echo "Now sleeping indefinitely to behave as a service"
sleep inf
