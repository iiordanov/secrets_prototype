# Docker Secrets as JSON String

Example Docker container image that has an ENTRYPOINT
that can parse the value of a SECRETS environment
variable as a JSON string and define each key/value pair
as an environment variables that it exports before executing CMD.

# Build

Run `docker build .`

```bash
$ docker build .
Sending build context to Docker daemon   42.5kB
Step 1/7 : FROM ubuntu:20.04
 ---> fb52e22af1b0
Step 2/7 : ENV SECRETS="{}"
 ---> Using cache
 ---> a1c4881e7049
Step 3/7 : RUN apt update && apt install -yy jq && apt autoremove && apt clean
 ---> Using cache
 ---> f55f4ad9d4cd
Step 4/7 : COPY unpack_secrets.sh /
 ---> Using cache
 ---> bad413583ec6
Step 5/7 : COPY command.sh /
 ---> Using cache
 ---> 6ecfb3f595ba
Step 6/7 : ENTRYPOINT [ "./unpack_secrets.sh" ]
 ---> Using cache
 ---> 9e482cf07d7c
Step 7/7 : CMD [ "./command.sh" ]
 ---> Using cache
 ---> e1fc38b8028a
Successfully built e1fc38b8028a
```

# Run

To run with default CMD defined:

```bash
docker run --env 'SECRETS={"ENV_VAR_ONE": "some value", "ENV_VAR_TWO": "some other value"}' --rm e1fc38b8028a
```

To override CMD with e.g. the `env` command:

```bash
docker run --env 'SECRETS={"ENV_VAR_ONE": "some value", "ENV_VAR_TWO": "some other value"}' --rm e1fc38b8028a env
```
