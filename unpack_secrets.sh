#!/bin/bash
#
# This script expects an environment variable SECRETS with value that is a json string

# For each key/value pair in the json string
for k in $(echo "${SECRETS}" | jq -r 'keys[]')
do
  # Parse the value out
  v=$(echo "${SECRETS}" | jq -r ".${k}")
  # Declare an environment varaible with the parsed key as name and parsed value as value.
  export "$k"="$v"
done

# Execute the parameters passed to this script as a sub-process (that
# has the secrets defined as environment variables)
exec $@
